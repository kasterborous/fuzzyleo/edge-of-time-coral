matproxy.Add({
    name = "TARDIS_eotcoral_RotorColor",

    init = function(self, mat, values)
        self.ResultTo = values.resultvar
    end,

    bind = function(self, mat, ent)
        if not IsValid(ent) or not ent.TardisPart then return end
        if not ent.interior then return end

        local col = ent.interior.metadata.Interior.Light.color


        col = Color(col.r, col.g, col.b):ToVector()



        mat:SetVector( self.ResultTo, col);
    end
})

matproxy.Add({
    name = "TARDIS_eotcoral_WallColor",

    init = function(self, mat, values)
        self.ResultTo = values.resultvar
    end,

    bind = function(self, mat, ent)
        if not IsValid(ent) or not ent.TardisPart then return end
        if not ent.interior then return end

        local col = ent.interior.metadata.Interior.Colors.Walls


        col = Color(col.r, col.g, col.b):ToVector()



        mat:SetVector( self.ResultTo, col);
    end
})

matproxy.Add({
    name = "TARDIS_eotcoral_RoundelColor",

    init = function(self, mat, values)
        self.ResultTo = values.resultvar
    end,

    bind = function(self, mat, ent)
        if not IsValid(ent) or not ent.TardisPart then return end
        if not ent.interior then return end

        local col = ent.interior.metadata.Interior.Colors.Roundels


        col = Color(col.r, col.g, col.b):ToVector()



        mat:SetVector( self.ResultTo, col);
    end
})

