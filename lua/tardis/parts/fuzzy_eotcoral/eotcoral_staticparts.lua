-- Adds Static Parts

local PART={}
PART.ID = "eotcoral_cables"
PART.Name = "eotcoral_cables"
PART.Model = "models/FuzzyLeo/EdgeOfReality/cables.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

--

local PART={}
PART.ID = "eotcoral_console"
PART.Name = "eotcoral_console"
PART.Model = "models/FuzzyLeo/EdgeOfReality/console.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_walldoor2"
PART.Name = "eotcoral_walldoor2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walldoor2.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_walldoor"
PART.Name = "eotcoral_walldoor"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walldoor.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_void"
PART.Name = "eotcoral_void"
PART.Model = "models/FuzzyLeo/EdgeOfReality/void.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_walldetail2"
PART.Name = "eotcoral_walldetail2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walldetail2.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_walldetail"
PART.Name = "eotcoral_walldetail"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walldetail.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_wall1"
PART.Name = "eotcoral_wall1"
PART.Model = "models/FuzzyLeo/EdgeOfReality/wall.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_wall2"
PART.Name = "eotcoral_wall2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/wall.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_wall3"
PART.Name = "eotcoral_wall3"
PART.Model = "models/FuzzyLeo/EdgeOfReality/wall.mdl"
PART.AutoSetup = true
PART.Collision = false

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_walkway"
PART.Name = "eotcoral_walkway"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walkway.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar1"
PART.Name = "eotcoral_pillar1"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar2"
PART.Name = "eotcoral_pillar2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar3"
PART.Name = "eotcoral_pillar3"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar4"
PART.Name = "eotcoral_pillar4"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar5"
PART.Name = "eotcoral_pillar5"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_pillar6"
PART.Name = "eotcoral_pillar6"
PART.Model = "models/FuzzyLeo/EdgeOfReality/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_hexagonbase"
PART.Name = "eotcoral_hexagonbase"
PART.Model = "models/FuzzyLeo/EdgeOfReality/hexagonbase.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_hexagonbase2"
PART.Name = "eotcoral_hexagonbase2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/hexagonbase2.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_consoledetails"
PART.Name = "eotcoral_consoledetails"
PART.Model = "models/FuzzyLeo/EdgeOfReality/consoledetail.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_consolerotor"
PART.Name = "eotcoral_consolerotor"
PART.Model = "models/FuzzyLeo/EdgeOfReality/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_chair"
PART.Name = "eotcoral_chair"
PART.Model = "models/FuzzyLeo/EdgeOfReality/chair.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_catwalk"
PART.Name = "eotcoral_catwalk"
PART.Model = "models/FuzzyLeo/EdgeOfReality/catwalk.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_catwalkclassic"
PART.Name = "eotcoral_catwalkclassic"
PART.Model = "models/FuzzyLeo/EdgeOfReality/catwalkclassic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_controls"
PART.Name = "eotcoral_controls"
PART.Model = "models/FuzzyLeo/EdgeOfReality/controls.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doorframe"
PART.Name = "eotcoral_doorframe"
PART.Model = "models/FuzzyLeo/EdgeOfReality/doorframe.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doorframe2"
PART.Name = "eotcoral_doorframe2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/doorframe2.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	hook.Add("SkinChanged", "eotcoral-doorframe2", function(ent,i)
		if ent.TardisExterior then
			local eotcoral_doorframe2=ent:GetPart("eotcoral_doorframe2")
			if IsValid(eotcoral_doorframe2) then
				eotcoral_doorframe2:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local eotcoral_doorframe2=ent.interior:GetPart("eotcoral_doorframe2")
				if IsValid(eotcoral_doorframe2) then
					eotcoral_doorframe2:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doorframeclassic"
PART.Name = "eotcoral_doorframeclassic"
PART.Model = "models/FuzzyLeo/EdgeOfReality/doorframeclassic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_platform"
PART.Name = "eotcoral_platform"
PART.Model = "models/FuzzyLeo/EdgeOfReality/platform.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_platformrail"
PART.Name = "eotcoral_platformrail"
PART.Model = "models/FuzzyLeo/EdgeOfReality/platformrail.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doorframe10th"
PART.Model = "models/FuzzyLeo/EdgeOfReality/walldoor10th.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	hook.Add("SkinChanged", "eotcoral-doorframe10th", function(ent,i)
		if ent.TardisExterior then
			local eotcoral_doorframe10th=ent:GetPart("eotcoral_doorframe10th")
			if IsValid(eotcoral_doorframe10th) then
				eotcoral_doorframe10th:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local eotcoral_doorframe10th=ent.interior:GetPart("eotcoral_doorframe10th")
				if IsValid(eotcoral_doorframe10th) then
					eotcoral_doorframe10th:SetSkin(i)
				end
			end
		end
	end)
end

TARDIS:AddPart(PART)