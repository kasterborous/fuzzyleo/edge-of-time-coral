-- Adds controls and animated parts

local PART={}
PART.ID = "eotcoral_consolerotor2"
PART.Name = "eotcoral_consolerotor2"
PART.Model = "models/FuzzyLeo/EdgeOfReality/rotor2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.ClientThinkOverride = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.4, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_demat"
PART.Name = "eotcoral_demat"
PART.Model = "models/FuzzyLeo/EdgeOfReality/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "FuzzyLeo/EdgeOfReality/CoolyDude/throttle1.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_cloak"
PART.Name = "eotcoral_cloak"
PART.Model = "models/FuzzyLeo/EdgeOfReality/dail1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_EggTimerTurnClick_Loop2.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_coords"
PART.Name = "eotcoral_coords"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK2.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doorlock"
PART.Name = "eotcoral_doorlock"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK3.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_doortoggle"
PART.Name = "eotcoral_doortoggle"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK4.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_enginerelease"
PART.Name = "eotcoral_enginerelease"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_fastreturn"
PART.Name = "eotcoral_fastreturn"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK4.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_flight"
PART.Name = "eotcoral_flight"
PART.Model = "models/FuzzyLeo/EdgeOfReality/lever1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "FuzzyLeo/EdgeOfReality/CoolyDude/lever1.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_float"
PART.Name = "eotcoral_float"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK4.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_hads"
PART.Name = "eotcoral_hads"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_handbrake"
PART.Name = "eotcoral_handbrake"
PART.Model = "models/FuzzyLeo/EdgeOfReality/lever4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "FuzzyLeo/EdgeOfReality/CoolyDude/lever2.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_isomorphic"
PART.Name = "eotcoral_isomorphic"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK2.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_lockingmechanism"
PART.Name = "eotcoral_lockingmechanism"
PART.Model = "models/FuzzyLeo/EdgeOfReality/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "FuzzyLeo/EdgeOfReality/CoolyDude/throttle2.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_manualcoords"
PART.Name = "eotcoral_manualcoords"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_music"
PART.Name = "eotcoral_music"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_power"
PART.Name = "eotcoral_power"
PART.Model = "models/FuzzyLeo/EdgeOfReality/dail2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 8
PART.Sound = "FuzzyLeo/EdgeOfReality/bigclick.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_redecoration"
PART.Name = "eotcoral_redecoration"
PART.Model = "models/FuzzyLeo/EdgeOfReality/dail2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 8
PART.Sound = "FuzzyLeo/EdgeOfReality/bigclick.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_repair"
PART.Name = "eotcoral_repair"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_scanners"
PART.Name = "eotcoral_scanners"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_screentoggle"
PART.Name = "eotcoral_screentoggle"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK3.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_sonicdispenser"
PART.Name = "eotcoral_sonicdispenser"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK4.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_thirdperson"
PART.Name = "eotcoral_thirdperson"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK5.mp3"

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eotcoral_vortexflight"
PART.Name = "eotcoral_vortexflight"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_Beep_DK4.mp3"

TARDIS:AddPart(PART)