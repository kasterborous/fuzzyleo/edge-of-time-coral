

local T={}
T.Base="edgeoftimecoral"
T.Name="EOT Coral 10th Exterior"
T.ID="edgeoftimecoral10th"

T.IsVersionOf = "edgeoftimecoral"

T.Interior={
	Portal = {
		-- Generated by portals debug tool
		pos = Vector(0, 344.9, 19.55),
		ang = Angle(-0, -90, 0),
		width = 68,
		height = 92,
		thickness = -5,
		inverted = false,
	},
	Sounds={
		Door={
			enabled=true,
			open="drmatt/tardis/door_open.wav",
			close="drmatt/tardis/door_close.wav"
		},
	},
	Parts={
		door = {
            Model="models/FuzzyLeo/newexteriorpack/10thbox/10thdoors.mdl",
            posoffset=Vector(29.5, 0, -45.7),
        },
		eotcoral_doorframe10th            =   {pos = Vector(0, 0, 0),                     ang = Angle(-0, 0, 0),               },
		eotcoral_doorframe2            =   false,
	},
	IntDoorAnimationTime = 0.6,
}

T.Exterior={

    Model = "models/FuzzyLeo/newexteriorpack/10thbox/10thshell.mdl",
    ScannerOffset = Vector(42,0,50),
    Fallback = {
        pos = Vector(50,0,5),
        ang = Angle(0,0,0)
    },
    DoorAnimationTime = 0.6,
    PhaseMaterial = "models/FuzzyLeo/boxcapsule/phasemat.vmt",
    Parts = {
        door = {
            model = "models/FuzzyLeo/newexteriorpack/10thbox/10thdoors.mdl",
            posoffset=Vector(-29.5, 0, -45.7)
        }
    },
	Sounds={
		Door={
			enabled=true,
			open="drmatt/tardis/door_open.wav",
			close="drmatt/tardis/door_close.wav"
		},
	},
    ProjectedLight = {
        --color = Color(r,g,b), --Base color. Will use main interior light if not set.
        --warncolor = Color(r,g,b), --Warning color. Will use main interior warn color if not set.
        brightness = 0.1, --Light's brightness
        vertfov = 90,
        horizfov = 60, --vertical and horizontal field of view of the light. Will default to portal height and width.
        farz = 450, --FarZ property of the light. Determines how far the light projects.]]
        offset = Vector(-18, 0, 40), --Offset from box origin
        texture = "effects/flashlight/square" --Texture the projected light will use. You can get these from the Lamp tool.
    },
    Light={
        enabled=true,
        pos=Vector(0, 0, 110),
        color = Color(255,218,138),
    },
    Portal = {
        -- Generated by portals debug tool
        pos = Vector(29.5, 0, 45.7),
        ang = Angle(0, 0, 0),
        width = 42,
        height = 83.5,
        thickness = 22,
        inverted = true,
    },
}

T.Interior.TextureSets = {
	["normal"] = {

		{ "eotcoral_doorframe", 0, "tardis_white" },
		{ "door", 1, "10windows" },

		{ "door", 5, "10windows2" },
		{ false },
		{ false },
		{ false },

	},
}


T.Exterior.TextureSets = {
	["normal"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/",

		{ "self", 2, "10pbsign" },

		{ "self", 1, "10windows" },
		{ "door", 1, "10windows" },

		{ "self", 4, "10lamp" },

		{ "self", 6, "10windows2" },
		{ "door", 5, "10windows2" },

	},
	["flight"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/",
		base = "normal",
	},
	["warning_flight"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/10thbox_warning/",
		base = "normal",
	},
	["poweroff"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/10thbox_off/",
		base = "normal",
	},
	["warning_off"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/10thbox_off/",
		base = "normal",
	},
	["warning"] = {
		prefix = "models/FuzzyLeo/newexteriorpack/10thbox/10thbox_warning/",
		base = "normal",
	},
}

T.Templates = {

    --Exterior override
    edgeoftime_box = false,

}


TARDIS:AddInterior(T)