TARDIS:AddInteriorTemplate("eotcoral_lightrotor_default", {
	Interior = {
		Light = {
			color = Color(0, 136, 220),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 1,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_dim", {
	Interior = {
		Light = {
			color = Color(0, 136, 220),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.3,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.2,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_orange", {
	Interior = {
		Light = {
			color = Color(255, 90, 0),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.3,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(255, 90, 0),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.4,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(255, 90, 0),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_deepblue", {
	Interior = {
		Light = {
			color = Color(0, 68, 255),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(0, 68, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 1,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(0, 68, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_purple", {
	Interior = {
		Light = {
			color = Color(50, 0, 255),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(50, 0, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.6,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(50, 0, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_green", {
	Interior = {
		Light = {
			color = Color(0, 255, 64),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.6,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_dimgreen", {
	Interior = {
		Light = {
			color = Color(0, 255, 64),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.2,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.1,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_white", {
	Interior = {
		Light = {
			color = Color(255, 255, 255),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.6,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_dimwhite", {
	Interior = {
		Light = {
			color = Color(255, 255, 255),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.2,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.1,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})


TARDIS:AddInteriorTemplate("eotcoral_lightrotor_off", {
	Interior = {
		Light = {
			color = Color(0, 0, 0),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0,
			nopower = false
		},
	},
})


TARDIS:AddInteriorTemplate("eotcoral_lightrotor_pink", {
	Interior = {
		Light = {
			color = Color(255, 121, 183),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(255, 121, 183),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 0.6,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(255, 121, 183),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightrotor_red", {
	Interior = {
		Light = {
			color = Color(255, 26, 26),
			warncolor = Color(255, 15, 15),
			pos = Vector(0, 0, 95),
			brightness = 0.5,
			nopower = false
		},

		Lamps = {
			consolelamp = {
				color = Color(255, 26, 26),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 540,
				brightness = 1,
				pos = Vector(0, 0, 120),
				ang = Angle(90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 540,
					brightness = 0.1,
				},
			},
			consolelamp2 = {
				color = Color(255, 26, 26),
				texture = "effects/flashlight/soft",
				fov = 130,
				distance = 0,
				brightness = 0,
				pos = Vector(0, 0, 80),
				ang = Angle(-90, 0, 0),
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
				nopower = true,
				off = {
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.5,
				},
				off_warn = {
					color = Color(255, 255, 255),
					enabled = true,
					fov = 60,
					distance = 100,
					brightness = 0.1,
				},
			},
		},
	},
})