TARDIS:AddInteriorTemplate("eotcoral_lightedge_default", {
	Interior = {

		Colors = {
			Walls = Color(255, 128, 48),
			Roundels = Color(255, 204, 77),
		},

		Lights = {
			{
				color = Color(255, 184, 53),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 2,
				nopower = false
			},
			{
				color = Color(255, 184, 53),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 2,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(255, 184, 53),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(255, 184, 53),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_dim", {
	Interior = {
		Colors = {
			Walls = Color(255, 128, 48),
			Roundels = Color(255, 204, 77),
		},
		Lights = {
			{
				color = Color(255, 184, 53),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 0.3,
				nopower = false
			},
			{
				color = Color(255, 184, 53),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 0.3,
				nopower = false
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_blue", {
	Interior = {

		Colors = {
			Walls = Color(0, 106, 172),
			Roundels = Color(162, 220, 255),
		},

		Lights = {
			{
				color = Color(0, 136, 220),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 2,
				nopower = false
			},
			{
				color = Color(0, 136, 220),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 2,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(0, 136, 220),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_deepblue", {
	Interior = {

		Colors = {
			Walls = Color(43, 72, 151),
			Roundels = Color(82, 128, 255),
		},

		Lights = {
			{
				color = Color(0, 45, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 2,
				nopower = false
			},
			{
				color = Color(0, 45, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 2,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(0, 45, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(0, 45, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_purple", {
	Interior = {

		Colors = {
			Walls = Color(68, 54, 190),
			Roundels = Color(172, 162, 255),
		},

		Lights = {
			{
				color = Color(25, 0, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 2,
				nopower = false
			},
			{
				color = Color(25, 0, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 2,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(25, 0, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(25, 0, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 2,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_green", {
	Interior = {

		Colors = {
			Walls = Color(60, 153, 83),
			Roundels = Color(171, 255, 192),
		},

		Lights = {
			{
				color = Color(0, 255, 64),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 1,
				nopower = false
			},
			{
				color = Color(0, 255, 64),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 1,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(0, 255, 64),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_white", {
	Interior = {

		Colors = {
			Walls = Color(94, 94, 94),
			Roundels = Color(255, 255, 255),
		},

		Lights = {
			{
				color = Color(255, 255, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 1,
				nopower = false
			},
			{
				color = Color(255, 255, 255),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 1,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(255, 255, 255),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_off", {
	Interior = {

		Colors = {
			Walls = Color(0, 0, 0),
			Roundels = Color(0, 0, 0),
		},

		LightOverride = {
			basebrightness = 0 ,
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_darkwhite", {
	Interior = {

		Colors = {
			Walls = Color(14, 14, 14),
			Roundels = Color(255, 255, 255),
		},

		LightOverride = {
			basebrightness = 0 ,
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_pink", {
	Interior = {

		Colors = {
			Walls = Color(94, 44, 68),
			Roundels = Color(255, 121, 183),
		},

		Lights = {
			{
				color = Color(255, 121, 183),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 1,
				nopower = false
			},
			{
				color = Color(255, 121, 183),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 1,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(255, 121, 183),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(255, 121, 183),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})

TARDIS:AddInteriorTemplate("eotcoral_lightedge_red", {
	Interior = {

		Colors = {
			Walls = Color(82, 32, 32),
			Roundels = Color(255, 123, 123),
		},

		Lights = {
			{
				color = Color(255, 26, 26),
				warncolor = Color(255, 15, 15),
				pos = Vector(-160, -100, 130),
				brightness = 1,
				nopower = false
			},
			{
				color = Color(255, 26, 26),
				warncolor = Color(255, 15, 15),
				pos = Vector(160, 100, 130),
				brightness = 1,
				nopower = false
			},
		},
		Lamps = {
			lamp1 = {
				color = Color(255, 26, 26),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(15, -15, 185),
				ang = Angle(0, -33, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
			lamp2 = {
				color = Color(255, 26, 26),
				texture = "effects/flashlight/soft",
				fov = 100,
				distance = 540,
				brightness = 1,
				pos = Vector(-15, 15, 185),
				ang = Angle(0, 147, 0),
				nopower = false,
				shadows = false,
				sprite = false,
				warn = {
					-- same list of options available
					sprite_brightness = 0,
					color = Color(255, 15, 15),
				},
			},
		},
	},
})