TARDIS:AddInteriorTemplate("eotcoral_defaultsnd", {
})

TARDIS:AddInteriorTemplate("eotcoral_coralsnd", {
	Interior = {
		IdleSound = {
			{
				path = "FuzzyLeo/EdgeOfReality/CoolyDude/coral_hum_loop.wav",
				volume = 1
			},
		},
		Sounds={
			FlightLoop = "FuzzyLeo/EdgeOfReality/CoolyDude/coral_inflight.wav",

		},
	},
	Exterior = {
		Parts = {
			vortex = {
				model = "models/doctorwho1200/coral/2005timevortex.mdl",
				pos = Vector(0,0,50),
				ang = Angle(0,0,0),
				scale =10
			},
		},
		Sounds = {
			Teleport = {
				demat = "FuzzyLeo/EdgeOfReality/CoolyDude/coral_demat.wav",
				mat = "FuzzyLeo/EdgeOfReality/CoolyDude/coral_mat.wav",
				mat_fast = "ambient/machines/teleport3.wav",
				mat_damaged_fast = "p00gie/tardis/mat_damaged_fast.wav",
				fullflight = "drmatt/tardis/full.wav",
				interrupt = "drmatt/tardis/repairfinish.wav",
			},
		},
		Teleport = {
			DematSequenceDelays={
				[1] = 4.5
			},
			MatSequenceDelays={
				[1] = 0
			},
		}
	},
})

TARDIS:AddInteriorTemplate("eotcoral_shake", {

	CustomHooks = {

		demat_shake_or_whatever = {
			exthooks = {["DematStart"] = true,},
			inthooks = {},
			func = function(ext, int)
				if SERVER then
					util.ScreenShake(int:GetPos(),5,100,20,700)
				end
			end,
		},
		failed_demat_shake = {
			exthooks = {["HandleNoDemat"] = true,},
			inthooks = {},
			func = function(ext, int)
				if SERVER then
					util.ScreenShake(int:GetPos(),5,100,5,700)
				end
			end,
		},
	},

})