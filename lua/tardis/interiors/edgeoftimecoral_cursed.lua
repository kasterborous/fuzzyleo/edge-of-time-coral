

local T={}
T.Base="edgeoftimecoral"
T.Name="EOT Coral Cursed"
T.ID="edgeoftimecoralcursed"

T.EnableClassicDoors = false

T.IsVersionOf = "edgeoftimecoral"

T.Interior={
    Portal = {
        -- Generated by portals debug tool
        pos = Vector(-0.30, 328.75, 23),
        ang = Angle(0, -90, 0),
        width = 42.3,
        height = 84.8,
        thickness = 22.5,
        inverted = 1,
    },
    Fallback = {
        pos = Vector(0, 310, -18),
        ang = Angle(0, 230, 0)
    },
    Parts = {
        door = {
            Model="models/FuzzyLeo/EdgeOfReality/ExteriorJodieDoorsINT.mdl",
            posoffset=Vector(-26.7, 0, -47),
        },
		eotcoral_pillar6            =   false,
		eotcoral_doorframe            =   {},
		eotcoral_doorframe2            =   false,
		eotcoral_walldoor            =   {},
		eotcoral_walldoor2            =   false,
		eotcoral_walldetail            =   {},
		eotcoral_walldetail2            =   false,
		eotcoral_hexagonbase            =   {},
		eotcoral_hexagonbase2            =   false,
		eotcoral_platformrail            =   {ang = Angle(-0, 0, 0),},
		eotcoral_catwalk            =   false,
	},
}

T.Interior.TextureSets = {
	["normal"] = {

                { "eotcoral_doorframe", 0, "tardis_white" },
		        { "door", 0, "tardis_white" },
			},
}


TARDIS:AddInterior(T)